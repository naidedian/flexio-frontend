import { GET_APP_STATE } from "../state";
import urls from "../utils/urls";
import fetcher from "../utils/fetcher";

export const isLoggedIn = () => GET_APP_STATE().AUTH_TOKEN !== undefined;
export const authenticate = credentials =>
  new Promise((resolve, reject) => {
    fetcher
      .post(urls.LOGIN, credentials)
      .then(response => {
        if (response.data.error)
          resolve({ error: { message: response.data.message } });
        else resolve(response.data);
      })
      .catch(error => resolve({ error }));
  });
