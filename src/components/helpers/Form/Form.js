import React from 'react'
import PropTypes from 'prop-types'
import Field from './Field/Field'
import Submit from './Submit/Submit'
import './Form.scss'

const getValidationFunctions = fields => Object.fromEntries(
    Object.entries(fields).map(entries => {
        const fieldKey = entries[0]
        const fieldValidatorFn = entries[1].validator
        return [fieldKey, fieldValidatorFn]
    }))

const getInvalidKeys = (values, fields) => {
    const keys = Object.keys(fields)
    const validationFunctions = getValidationFunctions(fields)
    return keys
        .map(k => {
            const currentValue = values[k]
            const validator = validationFunctions[k]
            if (validator(currentValue) === false) return k
            return null
        }).filter(v => v !== null)
}

const noop = () => {
    //DO NOTHING
}

class Form extends React.Component {

    static defaultProps = {
        getInvalidMessage: noop,
        onFieldsUpdated: noop,
        submit: noop
    }

    constructor(props) {
        super(props)
        this.submit = this.submit.bind(this)
        this.onFieldChange = this.onFieldChange.bind(this)
        this.state = {
            values: {},
            message: {type: undefined, value: undefined},
            invalid: new Set(),
            loading: false
        }
    }

    submit(event) {
        event.preventDefault()
        this.setState({loading: true})
        const invalid = new Set(getInvalidKeys(this.state.values, this.props.fields))
        if (invalid.size > 0)
            this.setState({
                loading: false,
                invalid,
                message: {
                    type: 'error',
                    value: this.props.getInvalidMessage(invalid)
                }
            })
        else this.props.submit(this)
    }

    onFieldChange(event) {
        // Clear errors
        this.state.invalid.delete(event.key)
        this.setState({message: {type: undefined, value: undefined}})

        // Set Change
        const values = this.state.values
        values[event.key] = event.value
        this.setState({values})
        this.props.onFieldsUpdated(values)
    }

    render() {
        const controlClasses = ``
        const messageClass = `message ${this.state.message.type || ''}`
        const getInputClasses = (invalid, key) => invalid.has(key) ? 'is-danger' : ''
        let submitButton = ''
        if (this.props.withSubmitButton) {
            submitButton = <Submit text={this.props.submitButtonText}
                                   withLoadingBar={this.props.withLoadingBar}
                                   loading={this.state.loading}
                                   onClick={this.submit}/>
        }


        return (
            <form className="Form" onSubmit={this.submit}>
                {
                    this.props.fields.map(f => {
                        return (
                            <Field key={f.name}
                                   name={f.name}
                                   label={f.label}
                                   type={f.type}
                                   placeholder={f.placeholder}
                                   controlClasses={f.controlClasses || controlClasses}
                                   inputClasses={getInputClasses(this.state.invalid, f.name)}
                                   onChange={this.onFieldChange}>
                                {
                                    f.icon ? <span className="icon is-small is-left"><i className={f.icon}/></span> : null
                                }
                            </Field>
                        )
                    })
                }

                {this.props.children}
                <div className={messageClass}>{this.state.message.value || ''}</div>
                {submitButton}
            </form>

        )
    }
}

Form.propTypes = {
    fields: PropTypes.array,
    withLoadingBar: PropTypes.bool,
    withSubmitButton: PropTypes.bool,
    submitButtonText: PropTypes.string,
    getInvalidMessage: PropTypes.func,
    onFieldsUpdated: PropTypes.func,
    submit: PropTypes.func
}

export default Form