import React from 'react'
import GoogleMapReact from 'google-map-react'
import PropTypes from 'prop-types'

const setMarker = (context, maps, map, location) => {
    if (context.state.marker) context.state.marker.setPosition(location)
    else context.setState({marker: new maps.Marker({position: location, map})})
}

const showAddressWindow = (context, maps, map, address) => {
    if (context.state.addressWindow) context.state.addressWindow.close()
    const addressWindow = new maps.InfoWindow()
    addressWindow.setContent(address)
    addressWindow.open(map, context.state.marker)
    context.setState({addressWindow})
}

const initiateSearchbar = (context, maps, map, input) => {
    const bounds = maps.LatLngBounds(maps.LatLng(-33.8902, 151.1759), maps.LatLng(-33.8474, 151.2631))
    const searchBox = maps.places.SearchBox(input, {bounds})

    searchBox.addListener('places_changed', () => {
        const places = searchBox.getPlaces()
        context.state.marker.setPosition(null)
        const bounds = maps.LatLngBounds()
        places.forEach(place => {
            // No "Geometry"
            if (!place.geometry) return

            const icon = {
                url: place.icon,
                size: maps.Size(71, 71),
                origin: maps.Point(0, 0),
                anchor: maps.Point(17, 34),
                scaledSize: maps.Size(25, 25)
            }

            context.state.marker.setPosition(place.geometry.position)
            context.state.marker.setIcon(icon)

            // Only geocodes have viewport.
            if (place.geometry.viewport) bounds.union(place.geometry.viewport)
            else bounds.extend(place.geometry.location)
        })

        map.fitBounds(bounds)
    })
}

class WaypointMap extends React.Component {
    static defaultProps = {
        center: {lat: 18.447991, lng: -66.065706},
        zoom: 14
    }

    constructor(props) {
        super(props)
        this.handleApiLoaded = this.handleApiLoaded.bind(this)
        this.state = {apiKey: 'AIzaSyCt-HxGAPXVL_IFMcfmhkvRqtBl0OMW6t0'}
    }

    handleApiLoaded(map, maps) {
        const handleMapClick = location => {
            const geocoder = new maps.Geocoder()
            geocoder.geocode({location}, (results, status) => {
                if (status === 'OK') {
                    const address = results[0].formatted_address
                    setMarker(this, maps, map, location)
                    showAddressWindow(this, maps, map, address)
                    map.panTo(location)
                    this.props.updateLocation(location, address)
                }
                else this.props.updateLocation()
            })
        }
        map.setOptions({draggableCursor: 'default'})
        maps.event.addListener(map, 'click', event => handleMapClick(event.latLng))
    }

    render() {
        return (
            <div className="WaypointMap">
                <div style={{height: '40em', width: '100%'}}>
                    <GoogleMapReact
                        defaultZoom={14}
                        defaultCenter={{lat: 18.447991, lng: -66.065706}}
                        bootstrapURLKeys={{key: 'AIzaSyCt-HxGAPXVL_IFMcfmhkvRqtBl0OMW6t0'}}
                        yesIWantToUseGoogleMapApiInternals
                        onGoogleApiLoaded={({map, maps}) => this.handleApiLoaded(map, maps)}
                    />
                </div>
            </div>
        )
    }
}

WaypointMap.propTypes = {
    center: PropTypes.object,
    zoom: PropTypes.number,
    updateLocation: PropTypes.func
}

export default WaypointMap
