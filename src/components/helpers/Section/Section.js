import React from 'react'
import PropTypes from 'prop-types'
import './Section.scss'

class Section extends React.Component {
    render() {
        return (
            <section id={this.props.id} className={`Section ${this.props.className || ''}`}>
                {this.props.title ? <div className="title">{this.props.title}</div> : null}
                {this.props.subTitle ? <div className="sub-title">{this.props.subTitle}</div> : null}
                {this.props.children}
            </section>
        )
    }
}

Section.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    title: PropTypes.string,
    subTitle: PropTypes.string,
}

export default Section
