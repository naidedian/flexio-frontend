import {GET_APP_STATE} from '../state'
import SPANISH from './spanish.locale'
import ENGLISH from './english.locale'

const locales = {SPANISH, ENGLISH}

export default locales[GET_APP_STATE().LOCALE] || ENGLISH
