const INITIAL_STATE = {
    LOCALE: 'SPANISH'
}

let APP_CONTEXT
export const GET_APP_STATE = () => JSON.parse(localStorage.getItem('flexio_state')) || INITIAL_STATE

export const SET_APP_STATE = newState => {
    APP_CONTEXT.setState(newState)
    localStorage.setItem('flexio_state', JSON.stringify(APP_CONTEXT.state))
}
export const CLEAR_APP_STATE = () => {
    APP_CONTEXT.setState(INITIAL_STATE)
    localStorage.setItem('flexio_state', JSON.stringify(INITIAL_STATE))
}

export const BUILD_INITIAL_STATE = appContext => {
    APP_CONTEXT = appContext
    if (GET_APP_STATE()) return GET_APP_STATE()
    else {
        localStorage.setItem('flexio_state', JSON.stringify(INITIAL_STATE))
        return INITIAL_STATE
    }
}
