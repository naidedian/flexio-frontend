import React, { Component } from "react";
import Container from "../../components/helpers/Container/Container";
import NewOrderWaypoint from "./NewOrderWaypoint/NewOrderWaypoint";
import locale from "../../locale/_locale";
import PickupWaypoint from "../../components/views/PickupWaypoint/PickupWaypoint";
import Field from "../../components/helpers/Form/Field/Field";
import Section from "../../components/helpers/Section/Section";
import { mdiCloseBox } from "@mdi/js";
import Icon from "../../components/helpers/Icon/Icon";
import NewOrderPricing from "./NewOrderPricing/NewOrderPricing";
import fetcher from "../../utils/fetcher";
import URL from "../../utils/urls";
import _ from "lodash";

import "./NewOrder.scss";

const priceSection = context => {
  if (context.state.calculating)
    return <progress className="progress is-small" max="100" />;

  if (context.state.fixedPrice && context.state.consolidatedPrice)
    return (
      <NewOrderPricing
        consolidatedPrice={context.state.consolidatedPrice}
        fixedPrice={context.state.fixedPrice}
        LOCALE={context.LOCALE}
      />
    );
  else
    return (
      <button
        id="calculate-estimate-button"
        className="action-btn"
        onClick={context.calculateEstimate}
      >
        {context.LOCALE.CalculateEstimateButton}
      </button>
    );
};

export default class extends Component {
  constructor(props) {
    super(props);
    this.LOCALE = locale.NewOrderLocale;

    this.addWaypoint = this.addWaypoint.bind(this);
    this.removeWaypoint = this.removeWaypoint.bind(this);
    this.addPackage = this.addPackage.bind(this);
    this.removePackage = this.removePackage.bind(this);
    this.assignPackage = this.assignPackage.bind(this);
    this.unassignPackage = this.unassignPackage.bind(this);
    this.updateWaypointLocation = this.updateWaypointLocation.bind(this);
    this.updatePickupLocation = this.updatePickupLocation.bind(this);
    this.calculateEstimate = this.calculateEstimate.bind(this);

    this.state = {
      newPackageField: null,
      waypoints: [],
      packages: [],
      packageIndex: 0,
      fieldChange: ""
    };
  }

  addWaypoint() {
    const waypoints = this.state.waypoints;
    waypoints.push({});
    this.setState({ waypoints });
  }

  removeWaypoint(index) {
    const waypoints = this.state.waypoints;
    waypoints.splice(index, 1);
    this.setState({ waypoints });
  }

  addPackage(event) {
    event.preventDefault();
    const packageIndex = this.state.packageIndex;
    if (this.state.newPackageField) {
      const packages = this.state.packages;
      packages.push({
        isAssigned: false,
        text: this.state.newPackageField,
        index: packageIndex
      });
      this.setState({ packages, packageIndex: packageIndex + 1 });
    }
  }

  updateWaypointLocation(index, location, address) {
    const waypoints = this.state.waypoints;
    waypoints[index].location = { lat: location.lat(), lng: location.lng() };
    waypoints[index].address = address;
    this.setState({ waypoints });
  }

  async calculateEstimate() {
    this.setState({ calculating: true });
    const waypoints = _.cloneDeep(this.state.waypoints);
    waypoints.unshift(this.state.pickupLocation || {});
    const waypointLocations = waypoints
      .filter(w => w.location)
      .map(w => w.location);
    const onlyOneWaypoint = waypoints.length === 1;
    const allWaypointsHaveLocations =
      waypoints.length > 0 && waypoints.length === waypointLocations.length;

    if (allWaypointsHaveLocations) {
      if (onlyOneWaypoint) {
        // TODO: CALCULATE ESTIMATE FOR ONE ENDPOINT
        alert("ONLY ONE WAYPOINT");
      } else {
        const response = await fetcher.post(URL.OPTIMIZE_ROUTES, { waypoints });
        // TODO: CALCULATE PRICES
        const fixedPrice = "";
        const consolidatedPrice = "";
      }
    } else alert(this.LOCALE.AllWaypointsNeedLocationErrorMessage);

    this.setState({ calculating: false });
  }

  removePackage(index) {
    const packages = this.state.packages;
    packages.splice(index, 1);
    this.setState({ packages });
  }

  assignPackage(packageIndex, waypointIndex) {
    const packages = this.state.packages;
    packages[packageIndex].isAssigned = true;
    packages[packageIndex].waypoint = waypointIndex;
    this.setState({ packages });
  }

  unassignPackage(packageIndex) {
    const packages = this.state.packages;
    packages[packageIndex].isAssigned = false;
    packages[packageIndex].waypoint = undefined;
    this.setState({ packages });
  }

  updatePickupLocation(loc, address) {
    const location = { lat: loc.lat(), lng: loc.lng() };
    const pickupLocation = { location, address };
    this.setState({ pickupLocation });
  }

  onFieldChange(event) {
    console.log(this.state.fieldChange);
    this.setState({
      fieldChange: event.value
    });
  }

  render() {
    console.log(this.state);
    return (
      <Container
        componentClass="NewOrder"
        title={this.LOCALE.Title}
        size="is-half"
        containerClasses="is-top-padded"
      >
        <Field
          name="order-title"
          label={this.LOCALE.NewOrderFields.Title.Label}
          placeholder={this.LOCALE.NewOrderFields.Title.Label}
          onChange={event => this.onFieldChange(event)}
        />

        <Section
          id="packages"
          className="parent"
          title={this.LOCALE.NewOrderFields.NewPackage.Title}
        >
          <form className="add-package-form">
            <Field
              label={this.LOCALE.NewOrderFields.NewPackage.Label}
              name="package"
              placeholder="--"
              onChange={({ value }) =>
                this.setState({ newPackageField: value })
              }
            />
            <div className="package-btn-wrapper">
              <div className="action-btn-aligner">
                <button className="action-btn" onClick={this.addPackage}>
                  {this.LOCALE.NewPackageButtonLabel}
                </button>
              </div>
            </div>
          </form>

          <Section
            className="packages-list"
            subTitle={this.LOCALE.PackagesListLabel}
          >
            <ol id="packages-list">
              {this.state.packages.map((pck, i) => {
                const shownWaypointIndex = pck.waypoint + 1;
                return (
                  <li key={i} className="package">
                    <div className="icon-aligner">
                      <div className="package-info">
                        {shownWaypointIndex >= 0 ? (
                          <span className="tag package-waypoint-tag">
                            {this.LOCALE.PackageWaypointLabel}{" "}
                            {shownWaypointIndex}
                          </span>
                        ) : (
                          ""
                        )}
                        <span
                          className={`package-text ${
                            pck.isAssigned ? "assigned" : ""
                          }`}
                        >
                          {pck.text}
                        </span>
                      </div>
                      <div
                        className="icon-action"
                        onClick={() => this.removePackage(i)}
                      >
                        <Icon
                          className="package-icon"
                          size="2rem"
                          path={mdiCloseBox}
                        />
                      </div>
                    </div>
                  </li>
                );
              })}
            </ol>
          </Section>
        </Section>

        <Section id="pickup" className="parent">
          <PickupWaypoint updatePickupLocation={this.updatePickupLocation} />
        </Section>

        <Section
          id="waypoints"
          className="parent"
          title={this.LOCALE.WaypointsLabel}
        >
          <button className="action-btn" onClick={this.addWaypoint}>
            {this.LOCALE.AddWaypointButton}
          </button>
          {this.state.waypoints.map((w, i) => {
            return (
              <NewOrderWaypoint
                key={i}
                index={i}
                remove={this.removeWaypoint}
                packages={this.state.packages}
                assignedPackages={w.assignedPackages}
                assignPackage={this.assignPackage}
                unassignPackage={this.unassignPackage}
                updateLocation={this.updateWaypointLocation}
              />
            );
          })}
        </Section>

        {priceSection(this)}
      </Container>
    );
  }
}
