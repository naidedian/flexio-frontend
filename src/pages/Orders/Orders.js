import React, { Component } from "react";
import Container from "../../components/helpers/Container/Container";
import locale from "../../locale/_locale";
import "./Orders.scss";

export default class extends Component {
  constructor(props) {
    super(props);
    this.LOCALE = locale.OrdersLocale;
    this.state = {
      orders: []
    };
  }

  componentDidMount() {
    const orders = [
      {
        name: "Order 1",
        description:
          "Lorem ipsum lorem lorem Lorem ipsum lorem lorem Lorem ipsum lorem lorem"
      },
      {
        name: "Order 2",
        description:
          "Lorem ipsum lorem lorem Lorem ipsum lorem lorem Lorem ipsum lorem lorem"
      },
      {
        name: "Order 3",
        description:
          "Lorem ipsum lorem lorem Lorem ipsum lorem lorem Lorem ipsum lorem lorem"
      }
    ];

    this.setState({
      orders
    });
  }

  render() {
    return (
      <Container title={this.LOCALE.Title} containerClasses={`is-top-padded`}>
        {/* <ul>
          {this.state.orders.map((item, index) => (
            <li key={index}>
              {item.name}
              <p>{item.description}</p>
            </li>
          ))}
        </ul> */}
      </Container>
    );
  }
}
