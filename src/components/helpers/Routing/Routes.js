import React from "react";
import { Redirect, Switch } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import Navbar from "../../views/Navbar/Navbar";

// Pages
import Login from "../../../pages/Login/Login";
import Orders from "../../../pages/Orders/Orders";
import NewOrder from "../../../pages/NewOrder/NewOrder";
import Settings from "../../../pages/Settings/Settings";

export default () => {
  return (
    <Switch>
      <PrivateRoute
        exact
        path="/orders/new"
        component={NewOrder}
        navbar={Navbar}
      />
      <PrivateRoute exact path="/orders" component={Orders} navbar={Navbar} />
      <PrivateRoute
        exact
        path="/settings"
        component={Settings}
        navbar={Navbar}
      />
      <PublicRoute exact path="/login" component={Login} />
      <Redirect to="/orders" />
    </Switch>
  );
};
