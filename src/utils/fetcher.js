import {GET_APP_STATE} from '../state'

const auth = () => {
    const auth = `Bearer ${GET_APP_STATE().AUTH_TOKEN}`
    return new Headers({
        'Content-Type': 'application/json',
        'Authorization': auth
    })
}

export default {
    toQueryString: d => Object.keys(d).map(k => `${k}=${d[k]}`).join('&'),

    get: (url) => {
        let fetched = fetch(url, {method: 'GET'})
        return fetched.then(response => response.json())
    },

    put: (url, body, headers = auth()) => {
        let fetched = fetch(url, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(body)
        })
        return fetched.then(response => response.json())
    },

    post: (url, body, headers = auth()) => {
        let fetched = fetch(url, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(body)
        })
        return fetched.then(response => response.json())
    },

    postImage: (url, body) => {
        const auth = `Bearer ${GET_APP_STATE().Login.token}`
        const headers = new Headers({'Authorization': auth})
        let fetched = fetch(url, {
            method: 'POST',
            headers,
            body
        })
        return fetched.then(response => response.json())
    },

    del: (url, body, headers = auth()) => {
        let fetched = fetch(url, {
            method: 'DELETE',
            headers: headers,
            body: JSON.stringify(body)
        })
        return fetched.then(response => response.json())
    }
}
