import React from 'react'
import PropTypes from 'prop-types'
import WaypointMap from '../WaypointMap/WaypointMap'
import locale from '../../../locale/_locale'
import './PickupWaypoint.scss'
import Section from '../../helpers/Section/Section'

class Pickup extends React.Component {

    constructor(props) {
        super(props)
        this.LOCALE = locale.PickupWaypointLocale
        this.locationUpdated = this.locationUpdated.bind(this)
        this.state = {
            contact: {},
            packages: []
        }
    }

    locationUpdated(location, address) {
        this.setState({location, address})
        this.props.updatePickupLocation(location, address)
    }

    render() {
        return (
            <Section className="PickupWaypoint" title={this.LOCALE.Waypoint}>
                <Section subTitle={this.LOCALE.NotesLabel}>
                    <textarea name="notes" placeholder={this.LOCALE.NotesLabel}/>
                </Section>

                <Section subTitle={this.LOCALE.MapLabel}>
                    <WaypointMap updateLocation={this.locationUpdated}/>
                </Section>
            </Section>
        )
    }
}

Pickup.propTypes = {
    updatePickupLocation: PropTypes.func
}

export default Pickup
