import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isLoggedIn } from "../../../services/auth.service";

export default class PublicRoute extends React.Component {
  render() {
    const { component, ...remainingProps } = this.props;
    return (
      <Route
        {...remainingProps}
        render={routeProps => {
          if (isLoggedIn())
            return (
              <Redirect
                to={{ pathname: "/", state: { from: routeProps.location } }}
              />
            );
          else {
            const final = Object.assign({}, routeProps, remainingProps);
            return React.createElement(component, final);
          }
        }}
      />
    );
  }
}
