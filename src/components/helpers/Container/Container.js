import React from 'react'
import './Container.scss'

export default props => {
    const header = props.title ? <h1 className="title">{props.title}</h1> : ''
    const containerClass = `Container columns is-centered ${props.containerClasses}`
    const columnClass = `column ${props.size || 'is-three-quarters'}`
    return (
        <div className={props.componentClass}>
            <div className={containerClass}>
                <div className={columnClass}>
                    {header}
                    {props.children}
                </div>
            </div>
        </div>
    )
}
