import React from 'react'
import Section from '../../../components/helpers/Section/Section'
import PropTypes from 'prop-types'

class NewOrderPricing extends React.Component {
    render() {
        return (
            <Section id="action" className="parent" title={this.props.LOCALE.PricesLabel}>
            </Section>
        )
    }
}

NewOrderPricing.propTypes = {
    fixedPrice: PropTypes.number,
    consolidatedPrice: PropTypes.number,
    LOCALE: PropTypes.object
}

export default NewOrderPricing