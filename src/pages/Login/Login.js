import React from "react";
import AppContext from "../../components/App/AppContext";
import { SET_APP_STATE } from "../../state";
import { authenticate } from "../../services/auth.service";
import locale from "../../locale/_locale";

import Container from "../../components/helpers/Container/Container";
import Field from "../../components/helpers/Form/Field/Field";
import Submit from "../../components/helpers/Form/Submit/Submit";

import logo from "../../assets/flexio-badge.png";
import "./Login.scss";

const getInvalidCredentials = credentials =>
  Object.keys(credentials).filter(k => !credentials[k]);
const getInputClasses = (invalid, key) => (invalid.has(key) ? "is-danger" : "");

class Login extends React.Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);
    this.LOCALE = locale.LoginLocale;
    this.login = this.login.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.state = {
      error: "",
      credentials: { email: undefined, password: undefined },
      invalid: new Set(),
      loading: false
    };
  }

  async login(event) {
    event.preventDefault();
    this.setState({ loading: true });
    const invalid = new Set(getInvalidCredentials(this.state.credentials));

    if (invalid.size > 0)
      this.setState({
        error: this.LOCALE.EmptyLoginFieldsMessage,
        loading: false,
        invalid
      });
    else {
      const response = await authenticate(this.state.credentials);
      if (response.error)
        this.setState({
          error: this.LOCALE.InvalidCredentialsMessage,
          loading: false
        });
      else {
        SET_APP_STATE({ AUTH_TOKEN: response.token });
        this.props.history.push("/orders");
      }
    }
  }

  onInputChange(event) {
    // Clear errors
    this.setState({ error: "" });
    this.state.invalid.delete(event.key);

    // Set Change
    const credentials = this.state.credentials;
    credentials[event.key] = event.value;

    this.setState({ credentials });
  }

  render() {
    return (
      <Container
        componentClass="Login"
        containerClasses="is-vcentered"
        size="is-one-quarter"
      >
        <div className="box">
          <div className="logo">
            <img src={logo} alt="" />
          </div>

          <form onSubmit={this.login}>
            <Field
              name="email"
              label={this.LOCALE.EmailLabel}
              type="email"
              placeholder={this.LOCALE.EmailPlaceholder}
              controlClasses="has-icons-left"
              inputClasses={getInputClasses(this.state.invalid, "email")}
              onChange={this.onInputChange}
            >
              <span className="icon is-small is-left">
                <i className="fas fa-envelope" />{" "}
              </span>
            </Field>

            <Field
              name="password"
              label={this.LOCALE.PasswordLabel}
              type="password"
              placeholder={this.LOCALE.PasswordPlaceholder}
              controlClasses="has-icons-left"
              inputClasses={getInputClasses(this.state.invalid, "password")}
              onChange={this.onInputChange}
            >
              <span className="icon is-small is-left">
                <i className="fas fa-lock" />{" "}
              </span>
            </Field>

            <div className="message">{this.state.error}</div>

            <Submit
              text={this.LOCALE.SubmitButton}
              withLoadingBar
              loading={this.state.loading}
              onClick={this.login}
            />
          </form>
        </div>
      </Container>
    );
  }
}

export default Login;
