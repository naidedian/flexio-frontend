import React from 'react'
import {Link} from 'react-router-dom'
import {mdiFileDocumentBoxOutline, mdiFileDocumentBoxPlusOutline, mdiSettingsOutline} from '@mdi/js'
import AppContext from '../../App/AppContext'
import {CLEAR_APP_STATE} from '../../../state'
import Icon from '../../helpers/Icon/Icon'
import locale from '../../../locale/_locale'
import logo from '../../../assets/flexio-badge.png'
import './Navbar.scss'

class Navbar extends React.Component {
    static contextType = AppContext

    constructor(props) {
        super(props)
        this.LOCALE = locale.NavbarLocale
        this.logout = this.logout.bind(this)
    }

    logout() {
        CLEAR_APP_STATE()
        this.props.history.push('/login')
    }

    render() {
        return (
            <div className="Navbar">
                <div className="logo"><img src={logo} alt="Flexio"/></div>
                <Link to="/orders"><Icon path={mdiFileDocumentBoxOutline} title={this.LOCALE.OrdersLinkTitle}/></Link>
                <Link to="/orders/new"><Icon path={mdiFileDocumentBoxPlusOutline} title={this.LOCALE.NewOrderLinkTitle}/></Link>
                <Link to="/settings"><Icon path={mdiSettingsOutline} title={this.LOCALE.SettingsLinkTitle}/></Link>
                <div className="logout" onClick={this.logout}>{this.LOCALE.LogOutText}</div>
            </div>
        )
    }
}

export default Navbar