import React from 'react'
import {BrowserRouter as Router} from "react-router-dom"
import {Provider} from './AppContext'
import {BUILD_INITIAL_STATE} from '../../state'
import Routes from '../helpers/Routing/Routes'

import './App.scss'

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = BUILD_INITIAL_STATE(this)
    }

    render() {
        return (
            <div className="App">
                <Provider value={this.state}>
                    <Router>
                        <Routes/>
                    </Router>
                </Provider>
            </div>
        )
    }
}
