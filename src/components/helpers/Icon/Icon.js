import React from 'react'
import Icon from '@mdi/react'
import './Icon.scss'
import PropTypes from 'prop-types'

class IconWithTitle extends React.Component {
    static defaultProps = {
        title: ''
    }

    render() {
        return (
            <div className="Icon">
                <div className="icon-container">
                    <Icon
                        className={`icon ${this.props.className}`}
                        path={this.props.path}
                        title={this.props.title || ''}
                        size={this.props.size || 2}
                        horizontal={this.props.horizontal}
                        vertical={this.props.vertical}
                        rotate={this.props.rotate || 0}
                        spin={this.props.spin}/>
                </div>

                {this.props.title
                    ? <div className="title">{this.props.title}</div>
                    : null
                }
            </div>
        )
    }
}

IconWithTitle.propTypes = {
    path: PropTypes.string,
    title: PropTypes.string,
    size: PropTypes.string,
    className: PropTypes.string,
    horizontal: PropTypes.bool,
    vertical: PropTypes.bool,
    spin: PropTypes.number
}

export default IconWithTitle
