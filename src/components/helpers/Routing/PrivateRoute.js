import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isLoggedIn } from "../../../services/auth.service";

const renderComponentWithMergedProps = (component, navbar, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return React.createElement(
    "div",
    {},
    React.createElement(navbar, finalProps),
    React.createElement(component, finalProps)
  );
};

const redirectToLoginPage = routeProps => (
  <Redirect
    to={{
      pathname: "/login",
      state: { from: routeProps.location }
    }}
  />
);

export default class PrivateRoute extends React.Component {
  render() {
    const { component, authorized, redirectTo, navbar, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={routeProps => {
          if (!isLoggedIn()) return redirectToLoginPage(routeProps);
          return renderComponentWithMergedProps(
            component,
            navbar,
            routeProps,
            rest
          );
        }}
      />
    );
  }
}
