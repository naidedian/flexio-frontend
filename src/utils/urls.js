const endpoint =
	process.env.NODE_ENV !== "production"
		? "http://localhost:8080"
		: "http://flexioapi.us-east-2.elasticbeanstalk.com";

const environment =
	process.env.NODE_ENV !== "production" ? "production" : "staging";

export default {
	LOGIN: `${endpoint}/api/auth/login`,
	SIGNUP: `${endpoint}/api/auth/register`,
	FORGOT_PASSWORD: `${endpoint}/forgot-password`,
	RESET_PASSWORD: `${endpoint}/reset-password`,
	OPTIMIZE_ROUTES: `${endpoint}/optimized-routes`
};
