import React, {Component} from 'react'
import Container from '../../components/helpers/Container/Container'
import locale from '../../locale/_locale'

import './Settings.scss'

export default class extends Component {
    constructor(props) {
        super(props)
        this.LOCALE = locale.SettingsLocale
    }

    render() {
        return (
            <Container title={this.LOCALE.Title} containerClasses={`is-top-padded`}>
            </Container>
        )
    }
}
