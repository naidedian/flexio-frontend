const ENGLISH = {
    LoginLocale: {
        EmailLabel: 'Email',
        EmailPlaceholder: 'Email',
        PasswordLabel: 'Password',
        PasswordPlaceholder: 'Password',
        SubmitButton: 'Login',
        EmptyLoginFieldsMessage: 'Please enter an email and password',
        InvalidCredentialsMessage: 'Invalid email or password'
    },
    NavbarLocale: {
        OrdersLinkTitle: 'Orders',
        NewOrderLinkTitle: 'New Order',
        SettingsLinkTitle: 'Settings',
        LogOutText: 'Log Out'
    },
    NewOrderLocale: {
        Title: 'New Order',
        AddWaypointButton: 'Add A WayPoint',
        CreateNewOrderButton: 'Create New Order',
        FailedCreatingNewOrderMessage: 'Could not create New Order\n Verify that all fields are valid',
        PackagesLabel: 'Add Package',
        NewOrderFields: {
            Title: {
                Label: 'New Order Title'
            },
            FirstName: {
                Label: 'First Name'
            }
        }
    },
    PickupWaypointLocale: {
        Waypoint: 'Waypoint',
        NotesLabel: 'Delivery Notes',
        PackagesLabel: 'Packages',
        MapLabel: 'Add Address on Map',
        ContactLabels: {
            Name: 'Contact Name',
            Phone: 'Contact Phone',
            Email: 'Contact Email'
        }
    },
    NewOrderWaypointLocale: {
        Waypoint: 'Waypoint',
        ContactInformationLabel: '',
        NotesLabel: 'Delivery Notes',
        PackagesLabel: 'Packages',
        MapLabel: 'Add Address on Map',
        ContactLabels: {
            Name: 'Contact Name',
            Phone: 'Contact Phone',
            Email: 'Contact Email'
        }
    },
    OrdersLocale: {
        Title: 'Orders'
    },
    SettingsLocale: {
        Title: 'Settings'
    }
}

export default ENGLISH