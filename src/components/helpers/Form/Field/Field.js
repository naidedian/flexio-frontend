import React from 'react'
import PropTypes from 'prop-types'

import './Field.scss'

class Field extends React.Component {
    static defaultProps = {
        label: '',
        placeholder: ''
    }

    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this)
    }

    onChange({target}) {
        const change = {}
        change['key'] = this.props.name
        change['value'] = target.value
        this.props.onChange(change)
    }

    render() {
        const controlClasses = this.props.controlClasses || ''
        const inputClasses = this.props.inputClasses || ''

        return (
            <div className="Field">
                <label htmlFor="" className="label">{this.props.label}</label>
                <div className={`control ${controlClasses}`}>
                    <input type={this.props.type}
                           className={`input ${inputClasses}`}
                           placeholder={this.props.placeholder}
                           onChange={this.onChange}
                    />
                    {this.props.children}
                </div>
            </div>
        )
    }
}

Field.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    controlClasses: PropTypes.string,
    inputClasses: PropTypes.string,
    onChange: PropTypes.func
}

export default Field