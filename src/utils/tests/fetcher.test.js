import subject from '../fetcher'

global.fetch = {};

test('toQueryString parses a JSON object into a query string', () => {
    let o = {Id: 1, Name: 'John'}
    expect(subject.toQueryString(o)).toBe('Id=1&Name=John')
})

test('get method executes a GET request', () => {
    fetch = jest.fn(() => ({then: jest.fn()}))
    let url = 'http://example.com'
    subject.get(url)
    expect(fetch).toHaveBeenCalledWith(url, {method: 'GET'})
})

test('put method executes a PUT request', () => {
    fetch = jest.fn(() => ({then: jest.fn()}))
    let url = 'http://example.com'
    let h = {'Content-Type': 'x-www-form-urlencoded'}
    let data = subject.toQueryString({Id: 1, Name: 'foo'})
    subject.put(url, data, h)
    expect(fetch).toHaveBeenCalledWith(url, {method: 'PUT', headers: h, body: JSON.stringify(data)})
})

test('post method executes a POST request', () => {
    fetch = jest.fn(() => ({then: jest.fn()}))
    let url = 'http://example.com'
    let h = {'Content-Type': 'x-www-form-urlencoded'}
    let data = subject.toQueryString({Id: 1, Name: 'foo'})
    subject.post(url, data, h)
    expect(fetch).toHaveBeenCalledWith(url, {method: 'POST', headers: h, body: JSON.stringify(data)})
})

test('delete method executes a DELETE request', () => {
    fetch = jest.fn(() => ({then: jest.fn()}))
    let url = 'http://example.com'
    let h = {'Content-Type': 'x-www-form-urlencoded'}
    let data = subject.toQueryString({Id: 1, Name: 'foo'})
    subject.del(url, data, h)
    expect(fetch).toHaveBeenCalledWith(url, {method: 'DELETE', headers: h, body: JSON.stringify(data)})
})
