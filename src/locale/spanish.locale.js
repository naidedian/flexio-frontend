const SPANISH = {
    LoginLocale: {
        EmailLabel: 'Correo Electrónico',
        EmailPlaceholder: 'Correo Electrónico',
        PasswordLabel: 'Contraseña',
        PasswordPlaceholder: 'Contraseña',
        SubmitButton: 'Iniciar Sesión',
        EmptyLoginFieldsMessage: 'Favor de proveer un correo electrónico y contraseña',
        InvalidCredentialsMessage: 'Correo electrónico y contraseña invalidos'
    },
    NavbarLocale: {
        OrdersLinkTitle: 'Ordenes',
        NewOrderLinkTitle: 'Nueva Orden',
        SettingsLinkTitle: 'Ajustes',
        LogOutText: 'Cerrar Sesión'
    },
    NewOrderLocale: {
        Title: 'Nueva Orden',
        AddWaypointButton: 'Añadir Punto de Entrega',
        CreateNewOrderButton: 'Crear Nueva Orden',
        NewPackageButtonLabel: 'Añadir Artículo',
        FailedCreatingNewOrderErrorMessage: 'No se pudo crear una Nueva Orden. Asegurese que todos los campos estén validos',
        AllWaypointsNeedLocationErrorMessage: 'Existe algún Punto de Entrega que no tiene dirección. Asegurese que todos los Puntos de Entrega tenga una localización seleccionada en el mapa respectivo.',
        PackagesListLabel: 'Artículos a Recoger',
        PackageWaypointLabel: 'Entrega #',
        WaypointsLabel: 'Puntos De Entrega',
        CalculateEstimateButton: 'Calcular Estimado',
        PricesLabel: 'Precio',
        NewOrderFields: {
            Title: {
                Label: 'Titulo de Orden'
            },
            FirstName: {
                Label: 'Primer Nombre'
            },
            NewPackage: {
                Title: 'Artículos',
                Label: 'Identificador de Artículo'
            }
        }
    },
    PickupWaypointLocale: {
        Waypoint: 'Recogido',
        NotesLabel: 'Notas De Recogido',
        PackagesLabel: 'Artículos',
        MapLabel: 'Dirección de Recogido',
    },
    NewOrderWaypointLocale: {
        Waypoint: 'Entrega #',
        ContactInformationLabel: 'Información Del Contacto',
        NotesLabel: 'Notas De Entrega',
        PackagesLabel: 'Artículos',
        AssignedPackagesList: 'Artículos Designados',
        AddPackagesLabel: 'Añadir Artículo',
        MapLabel: 'Escoger Dirección en Mapa',
        ContactLabels: {
            Name: 'Nombre de Contacto',
            Phone: 'Numero de Contacto',
            Email: 'Correo Electrónico de Contacto',
        }
    },

    OrdersLocale: {
        Title: 'Ordenes'
    },
    SettingsLocale: {
        Title: 'Ajustes'
    }
}

export default SPANISH
