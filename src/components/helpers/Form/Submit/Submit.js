import React from 'react'
import PropTypes from 'prop-types'

import './Submit.scss'

class Submit extends React.Component {
    render() {
        const controlClasses = this.props.controlClasses || ''
        const buttonClasses = this.props.buttonClasses || ''

        return (
            <div className="field Submit">
                <div className={`control ${controlClasses}`}>
                    {
                        this.props.withLoadingBar && this.props.loading
                            ? <progress className="progress is-info" max="100"/>
                            : <button className={`button ${buttonClasses}`}
                                      onClick={this.props.onClick}>{this.props.text}</button>

                    }

                </div>
            </div>
        )
    }
}

Submit.propTypes = {
    text: PropTypes.string,
    controlClasses: PropTypes.string,
    buttonClasses: PropTypes.string,
    withLoadingBar: PropTypes.bool,
    loading: PropTypes.bool
}

export default Submit