import React from 'react'
import {mdiPlusBox, mdiMinusBox, mdiCloseBox} from '@mdi/js'
import PropTypes from 'prop-types'
import Icon from '../../../components/helpers/Icon/Icon'
import WaypointMap from '../../../components/views/WaypointMap/WaypointMap'
import Form from '../../../components/helpers/Form/Form'
import locale from '../../../locale/_locale'
import './NewOrderWaypoint.scss'
import Section from '../../../components/helpers/Section/Section'

const FIELDS = LOCALE => [
    {
        type: 'text',
        name: 'name',
        label: LOCALE.ContactLabels.Name,
        placeholder: LOCALE.ContactLabels.Name,
        validator: v => !!v
    },
    {
        type: 'tel',
        name: 'phone',
        label: LOCALE.ContactLabels.Phone,
        placeholder: LOCALE.ContactLabels.Phone,
        validator: v => !!v
    },
    {
        type: 'email',
        name: 'email',
        label: LOCALE.ContactLabels.Email,
        placeholder: LOCALE.ContactLabels.Email,
        validator: v => !!v
    }
]

class NewOrderWaypoint extends React.Component {

    constructor(props) {
        super(props)
        this.LOCALE = locale.NewOrderWaypointLocale
        this.currentPackageIndex = 0
        this.toggle = this.toggle.bind(this)
        this.fieldsUpdated = this.fieldsUpdated.bind(this)
        this.locationUpdated = this.locationUpdated.bind(this)
        this.state = {
            shownIndex: this.props.index + 1,
            isOpen: true,
            contact: {}
        }
    }

    toggle() {
        const isOpen = !this.state.isOpen
        this.setState({isOpen})
    }

    fieldsUpdated(values) {
        console.log(JSON.stringify(values))
    }

    locationUpdated(location, address) {
        this.setState({location, address})
        this.props.updateLocation(this.props.index, location, address)
    }

    render() {
        const isOpen = this.state.isOpen ? 'open' : 'closed'
        const openIcon = this.state.isOpen ? mdiMinusBox : mdiPlusBox
        const assignedPackages = this.props.packages.filter(p => p.waypoint === this.props.index)
        const shouldShowAssignPackagesSelection = this.props.packages.filter(p => !p.isAssigned).length > 0
        const shouldShowAssignedPackagesTitle = shouldShowAssignPackagesSelection || assignedPackages.length > 0
        return (
            <div className="NewOrderWaypoint">

                <div className={`title-container`}>
                    <div onClick={this.toggle}><Icon size="2rem" path={openIcon}/></div>
                    <div className="title">{this.LOCALE.Waypoint} {this.state.shownIndex}</div>
                    <div className="remove is-pulled-right" onClick={this.props.remove}><Icon size="2rem" path={mdiCloseBox}/></div>
                </div>

                <div className={`content ${isOpen}`}>

                    <Section className="contact-info" subTitle={this.LOCALE.ContactInformationLabel}>
                        <Form fields={FIELDS(this.LOCALE)} onFieldsUpdated={this.fieldsUpdated}/>
                    </Section>

                    <Section className="notes" subTitle={this.LOCALE.NotesLabel}>
                        <textarea placeholder={this.LOCALE.NotesLabel}/>
                    </Section>

                    <Section className="packages" subTitle={shouldShowAssignedPackagesTitle ? this.LOCALE.AssignedPackagesList : null}>
                        {
                            shouldShowAssignPackagesSelection ?
                                <div className="assign-packages">
                                    <div className="created-packages">
                                        <select className="created-packages" ref={input => this.currentPackageIndex = input}>
                                            {this.props.packages.map(p => {
                                                if (!p.isAssigned) return <option key={p.index} value={p.index}>{p.text}</option>
                                                else return ''
                                            })}
                                        </select>
                                    </div>
                                    <div className="assign-package-btn" onClick={() => this.props.assignPackage(this.currentPackageIndex.value, this.props.index)}>

                                        <button className="action-btn">{this.LOCALE.AddPackagesLabel}</button>
                                    </div>
                                </div>
                                : null
                        }

                        <ul className="assigned-packages">
                            {assignedPackages.map((ap, i) =>
                                <li key={i} className="assigned-package">
                                    <div className="icon-aligner">
                                        <span className={`assigned-package-text ${ap.isAssigned ? 'assigned' : ''}`}>{ap.text}</span>
                                        <div className="icon-action" onClick={() => this.props.unassignPackage(ap.index)}>

                                            <Icon className="package-icon" size="2rem" path={mdiCloseBox}/>
                                        </div>
                                    </div>
                                </li>)
                            }
                        </ul>
                    </Section>

                    <Section className="map" subTitle={this.LOCALE.MapLabel}>
                        <WaypointMap updateLocation={this.locationUpdated}/>
                    </Section>
                </div>

            </div>
        )
    }
}

NewOrderWaypoint.propTypes = {
    index: PropTypes.number,
    packages: PropTypes.array,
    remove: PropTypes.func,
    assignPackage: PropTypes.func,
    unassignPackage: PropTypes.func,
    updateLocation: PropTypes.func
}

export default NewOrderWaypoint
